#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "model.h"

#ifdef __linux__
#include <getopt.h> /* for getopt on Linux systems */
#endif

#define MAX_FILENAME_SIZE 128 /* Maximum size of a filename */
#define MAX_ARTICLE_SIZE 1000000
#define MAX_BUFFER_SIZE 1000000

unsigned int Input_file;
char Input_filename[MAX_FILENAME_SIZE];
unsigned int Output_file;
char Output_filename[MAX_FILENAME_SIZE];

size_t n_read;
char buffer[MAX_BUFFER_SIZE];

void usage(void)
{
    fprintf(stderr,
            "Usage: prep [options]\n"
            "\n"
            "Options:\n"
            "-i fn\tinput filename=fn (Required)"
            "-o fn\toutput filename=fn (Required)");
    exit(2);
}

void init_arguments(int argc, char *argv[])
{
    int opt;
    extern char *optarg;
    extern int optind;

    boolean Input_found = FALSE, Output_found = FALSE;

    /* get the argument options */

    while ((opt = getopt(argc, argv, "i:o:")) != -1)
        switch (opt)
        {
        case 'i':
            Input_found = TRUE;
            sprintf(Input_filename, "%s", optarg);
            break;
        case 'o':
            Output_found = TRUE;
            sprintf(Output_filename, "%s", optarg);
            break;
        default:
            usage();
        }

    if (!Input_found || !Output_found)
    {
        usage();
        exit(1);
    }

    for (; optind < argc; optind++)
        usage();
}

void remove_html_tags(char *src, char *dest)
{
    char *p1 = src;
    char *p2 = dest;
    while (*p1)
    {
        if (*p1 == '<')
        {
            while (*p1 && *p1 != '>')
                p1++;
            if (*p1)
                p1++;
        }
        else
        {
            *p2++ = *p1++;
        }
    }
    *p2 = '\0';
}

void remove_metadata(char *src, char *dest)
{
    char *p1 = src;
    char *p2 = dest;
    while (*p1)
    {
        if (strncmp(p1, "--", 2) == 0)
        {
            while (*p1 && *p1 != '\n')
                p1++;
            if (*p1)
                p1++;
        }
        else
        {
            *p2++ = *p1++;
        }
    }
    *p2 = '\0';
}

void remove_urls(char *src, char *dest)
{
    char *p1 = src;
    char *p2 = dest;
    while (*p1)
    {
        if (strncmp(p1, "http", 4) == 0)
        {
            while (*p1 && !isspace(*p1))
                p1++;
        }
        else
        {
            *p2++ = *p1++;
        }
    }
    *p2 = '\0';
}

void remove_unwanted_symbols(char *src, char *dest)
{
    char *p1 = src;
    char *p2 = dest;
    while (*p1)
    {
        if (isalnum(*p1) || isspace(*p1) || *p1 == '.' || *p1 == ',' || *p1 == '-' || *p1 == '\'' || *p1 == '(' || *p1 == ')' || *p1 == '$' || *p1 == '"')
        {
            *p2++ = *p1;
        }
        p1++;
    }
    *p2 = '\0';
}

void preprocess_article(char *article, char *cleaned_article)
{
    char temp1[MAX_ARTICLE_SIZE];
    char temp2[MAX_ARTICLE_SIZE];

    remove_html_tags(article, temp1);

    remove_metadata(temp1, temp2);

    remove_urls(temp2, temp1);

    remove_unwanted_symbols(temp1, cleaned_article);
}

int read_file(const char *filename, char *buffer, size_t buffer_size)
{
    FILE *file = fopen(filename, "r");
    if (!file)
    {
        perror("fopen");
        return -1;
    }

    n_read = fread(buffer, 1, buffer_size - 1, file);
    if (ferror(file))
    {
        perror("fread");
        fclose(file);
        return -1;
    }

    buffer[n_read] = '\0';
    fclose(file);
    return 0;
}

int write_file(const char *filename, const char *buffer)
{
    FILE *file = fopen(filename, "w");
    if (!file)
    {
        perror("fopen");
        return -1;
    }

    if (fputs(buffer, file) == EOF)
    {
        perror("fputs");
        fclose(file);
        return -1;
    }

    fclose(file);
    return 0;
}

void preprocess_file(const char *input_filename, const char *output_filename)
{
    char article[MAX_ARTICLE_SIZE];
    char cleaned_article[MAX_ARTICLE_SIZE];

    if (read_file(input_filename, article, sizeof(article)) < 0)
    {
        fprintf(stderr, "Error reading input file\n");
        return;
    }

    preprocess_article(article, cleaned_article);

    if (write_file(output_filename, cleaned_article) < 0)
    {
        fprintf(stderr, "Error writing output file\n");
        return;
    }
}

int main(int argc, char *argv[])
{
    printf("Preprocessing file\n");
    init_arguments(argc, argv);
    printf("Input file: %s\n", Input_filename);
    preprocess_file(Input_filename, Output_filename);
    printf("Preprocessing done\n");

    exit(0);
}
