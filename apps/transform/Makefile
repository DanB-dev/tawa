include ../../CONFIG

BIN = bin
OBJDIR = obj

all: $(BIN)/markup

# Create directories
$(OBJDIR):
	mkdir -p $(OBJDIR)

$(BIN):
	mkdir -p $(BIN)

# For general executables
$(BIN)/%: $(OBJDIR)/%.o | $(BIN)
	$(CC) -o $@ $^ $(LDFLAGS)

# For C files
$(OBJDIR)/%.o: %.c | $(OBJDIR)
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf $(OBJDIR) $(BIN)

# Demos for the markup program

# This is a simple example of how to use the markup program (Note: the input files are not included in the repository)
markup-demo : $(BIN)/markup
	$(BIN)/markup -m ../../data/models/REUTERS.dat < ../../data/input/testing-samples/r-test-1.txt -p 1000 -V -F


# This is a simple example of how to compile the markup program to WebAssembly. (Note: EMCC is required)
# For Information on the flags used, see: https://emscripten.org/docs/tools_reference/emcc.html
compile-emcc: markup.c
	emcc $< $(FILES) \
		-o markup.js \
		-s WASM=1 \
		-s EXPORTED_FUNCTIONS='["_main"]' \
		-s EXPORTED_RUNTIME_METHODS='["callMain","FS_readFile","FS"]' \
		-s INVOKE_RUN=0 \
		-s EXIT_RUNTIME=1 \
		-s MODULARIZE=1 \
		-s EXPORT_NAME="'Markup'" \
		-s FILESYSTEM=1 \
		-s FORCE_FILESYSTEM=1 \
		-s ENVIRONMENT=node \
		-s ALLOW_MEMORY_GROWTH=1 \
		-s INITIAL_MEMORY=536870912 \
		-s ASSERTIONS=1 \
		-O3 \
		$(INCLUDES)

.PHONY: all clean markup-demo compile-emcc