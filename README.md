# Tawa Toolkit

#### A Note From Preben

Tawa, or Text Analysis from Waikato - Toolkit, is a package created by Bill Teahan.
While the toolkit is quite useful and contains a lot of features, the tar distribution is a bit messy.
In order to make it more accessible, I have been working on cleaning up the repository and making it source control friendly.

Please see the COPYRIGHT file for the copyright notice.

If you have any problems with this repository, please feel free to submit an issue.

The persons who maintain the Git repository are:

- William Teahan &lt;wjteahan@gmail.com&gt; (Main Contributor) - Inactive
- Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt; (Contributor/ Maintainer) - Active
- Dan Boyes &lt;dan.boyes@bangor.ac.uk&gt; (Contributor/ Maintainer) - Active

### Model Data File Naming Convention

In this project, `.dat` files are used to store information about language models. To ensure clarity and uniqueness in naming, we follow a naming convention that incorporates distinctive elements based on the languages included.

**Convention:**

- **Format**: `<Identifier>.dat`
- **Identifier**: Use a combination of initials or abbreviations that uniquely identify the languages.

**Example**: If the languages included are "English" and "Spanish", the file could be named `EN-SP.dat`.

This convention helps in quickly identifying which languages are stored in each `.dat` file while mitigating the risk of confusion due to overlapping initials.

---

By using a combination of initials or abbreviations separated by a delimiter like `-`, you can create more distinct filenames that clearly indicate the languages involved. This approach maintains the compact format while addressing potential naming conflicts effectively. Adjust the example and specifics according to the languages and preferences in your project.

### Example of .dat File Contents

A `.dat` file typically contains entries that associate language names with their corresponding model paths. Each entry is formatted as follows:

```
English ./models/english.model
Maori ./models/maori.model
```

In this example:

- `English` is the name of the language model.
- `./models/english.model` is the relative path to the model file.

This structure allows for straightforward referencing and retrieval of language models used in the project.

---

Feel free to adjust the paths and language names in the example to match the actual structure and naming conventions used in your project.

## Directories:

A rough layout of the directories. Be mindful that some of these will only appear after initial setup is complete.

```
📁 Tawa/
└── Source code for Tawa API

📁 config/
└── Configuration files (CONFIG, INSTALL, README)

📁 data/
├── 📁  input/ Store input text files here
├── 📁  output/ Store output text files here
└── 📁  models/ Store models and dat files here

📁 demo/
└── Makefiles for demo models

📁 doc/
└── Documentation about Tawa
    ├── 📄 LaTeX/
    └── 📄 PostScript/

📁 include/
└── Header files for compilation

📁 lib/
└── Libraries used for compilation

📁 apps/
└── Sample demo applications and their source code
```

## Completed Refactor

- [x] Main Configs / Makefiles
- [x] Training Module
- [ ] Transform Module
     - [x] Markup
     - [ ] Segment
- [ ] Translate Module
- [ ] Classify Module
     - [x] Classify
     - [ ] ident_word
     - [ ] leave_one_out
- [x] Encode Module

## How do I install it?

To be Added.

## What does it do?

To be Added.

#### A Note From William

A few people have asked about the API for statistical modelling I'm
currently writing. It's still in the development stages, but I can
give a short example to illustrate how it works.

There are three main types of objects:

    1) model
         A number associated with a statistical model e.g. one model might
         have been trained on French text, anotGher on English text etc.
    2) symbol
         A number associated with a symbol in the alphabet (the API
         treats the symbols as unsigned integers; it doesn't know anything
         about what the symbols stands for i.e. whether they are ASCII characters,
         letters in the English alphabet, hieroglyphics, or whatever).
    3) context
         A number associated with the prediction context; this context is
         is updated after each symbol has been processed, and is used to make
         a prediction for subsequent symbols.

In the toolkit's libraries, there are routines to load a static model (from a file
on disk), create dynamic models, create a context associated with a model, step
through the probability distribution for predicting the next symbol given the current
context etc.

Experiments I did for my Ph.D. thesis based on using PPM text
compression models showed that the language could be identified using
this approach with almost 100% accuracy (it was even able to detect
the difference between samples of American and British English text
with 100% accuracy by training two models on text contained in the
Brown and LOB corpora).

Bill Teahan
2017
(wjteahan@gmail.com)
